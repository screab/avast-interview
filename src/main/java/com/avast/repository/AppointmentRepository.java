package com.avast.repository;

import com.avast.domain.Appointment;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Appointment entity.
 */
@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
    @Query("select appointment from Appointment appointment where appointment.user.login = ?#{principal.username}")
    List<Appointment> findByUserIsCurrentUser();

    @Query(
        value = "select distinct appointment from Appointment appointment left join fetch appointment.users",
        countQuery = "select count(distinct appointment) from Appointment appointment"
    )
    Page<Appointment> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct appointment from Appointment appointment left join fetch appointment.users")
    List<Appointment> findAllWithEagerRelationships();

    @Query("select appointment from Appointment appointment left join fetch appointment.users where appointment.id =:id")
    Optional<Appointment> findOneWithEagerRelationships(@Param("id") Long id);
}
