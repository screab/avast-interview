/**
 * View Models used by Spring MVC REST controllers.
 */
package com.avast.web.rest.vm;
