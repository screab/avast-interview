import * as dayjs from 'dayjs';
import { IUser } from 'app/entities/user/user.model';

export interface IAppointment {
  id?: number;
  name?: string | null;
  scheduleDate?: dayjs.Dayjs;
  user?: IUser | null;
  users?: IUser[] | null;
}

export class Appointment implements IAppointment {
  constructor(
    public id?: number,
    public name?: string | null,
    public scheduleDate?: dayjs.Dayjs,
    public user?: IUser | null,
    public users?: IUser[] | null
  ) {}
}

export function getAppointmentIdentifier(appointment: IAppointment): number | undefined {
  return appointment.id;
}
